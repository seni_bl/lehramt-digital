# "Digitale Basiskompetenzen im Lehramt"

Im Rahmen der Werkstatt des #OERcamp am LISUM im November 2019 hat die Community "Digitale Basiskompetenzen im Lehramt" begonnen, offene Materialien für den Einsatz im Lehramtsstudium zu erstellen. Zur Community gehören

* Torben Mau
* Ines Bieler
* Marie Güntzel
* Steffen Rörtgen
* Julia Kruse
* Sarah Martin
* Axel Klinger

![Ergebnisse Brainstorming](images/brainstorming.jpg)

## Kompetenzmodelle (Torben)

[Link zum Dokument](https://kurzelinks.de/l2d2-Kompetenz)

## Dein Startblock auf dem Weg ins Tool- und App-Universum (Julia)

Die folgenden Blöcke drehen sich jeweils um Tools zu einem Aspekt der Unterrichtsgestaltung und laden dich zu einer Erkundungsreise ein, aus der du mit einer Reihe an Kompetenzen ausgestattet gehen sollst. Behalte bei der Bearbeitung der Blöcke im Hinterkopf: Zu zeitgemäßem Lernen gehört nicht vordergründig ein großes mediales Feuerwerk. Vielmehr stehen Konzepte wie kollaboratives, problemlösungsorientiertes und selbstgesteuertes Lernen im Vordergrund. Auch wenn du dies in deiner eigenen Schulbiografie vielleicht nicht erlebt hast - versuche,  einen offenen Geist mitzubringen und dich auf eine neue Art des Lernens einzulassen. Die vorgestellten Tools sollen dich und deine Lernenden bei diesem zeitgemäßen Lernen unterstützen.

**Das klingt echt kompliziert! Schaff ich das überhaupt?**
Die gute Nachricht: Du sollst dich auf diese Reise ausdrücklich *nicht* alleine begeben! Werde zum Teamsportler und lerne gemeinsam mit deinen Kommiliton*innen.

Für weitere Inspiration steht dir außerdem offen, die Möglichkeiten von Social Media und Internet zu nutzen. Eine Übersicht mit zahlreichen Blogs, Video-Tutorials und Hilfestellungen für die Nutzung von Twitter findest du weiter unten. Dort wird dir in sehr konkreter Form ein Einblick gegeben, wie Lehrer*innen in der Praxis zeitgemäßes Lernen gestalten.
Nun viel Vergnügen!

(*Hinweis für Dozierende*: Die Studierenden sollten motiviert werden, ihre Learnings sowie entstehende Produkte zu dokumentieren, zu teilen und in einen kommunikativen Austausch zu treten. Dies ist zB in Form von Blogs, Etherpads und der Nutzung bereits bestehender Lernplattformen möglich. Für die Studierenden ist dies von großer Bedeutung; sie erhalten Feedback, üben sich in Feedback-Geben, eignen sich die Kultur des Teilens und der Kollaboration durch direktes Erleben an und der Aspekt der Nutzbarkeit rückt deutlich stärker in den Vordergrund. Arbeitsergebnissen soll so eine größere Relevanz zuteil werden, wenn möglich über den Seminarkontext hinaus.)

### Deine Lernziele (aufbauend auf DigCompEdu):

Das kann ich nach dem fleißigen Absolvieren dieses Moduls:

* 2.1 und 2.2 Digitale Ressourcen:
    * Auswählen, Erstellen und Anpassen digitaler Ressourcen

* 3.1 Lehren und 3.3 Kollaboratives Lernen und Lehren:
    * Einsatz digitaler Geräte/Materialien planen und gestalten
    * Unterrichtsmethoden einbetten, organisieren und gestalten
    * Kollaborative Lernstrategien fördern und verbessern

* 4.3 Feedback und Planung:
    * Lernenden gezielt und zeitnah Feedback geben

*Quelle:* <https://ec.europa.eu/jrc/en/digcompedu>

---

### Basis

Die Aufgabenblöcke bauen auf dieser **dreiteiligen Padlet-Reihe** auf:

* <https://padlet.com/ajoth1/lw122tw6u4oh>
* <https://padlet.com/ajoth1/qk5gjl0n6utq>
* <https://padlet.com/ajoth1/h6v0jkfm5nwk>

**Hilfestellung**

Didaktische Unterstützung für diverse Tools:

* <https://padlet.com/j_vedder/digitalemethoden>
* <https://bayernedu.net/index.php/2018/03/25/alle-dibis-downloads-auf-einen-blick/>

Video-Tutorials zu diversen Tools:

* <https://www.flippedmathe.de/fortbildung/digi-ws/>
* <https://deutsches-lehrerforum.de/webinare/>

**Blogs** aus dem deutschsprachigen Raum:

* <https://tollerunterricht.com/> (L für Geschichte/Latein/Info)
* <https://diefraumitdemdromedar.de> (L für Deutsch/Geschichte/Sozi/Mathe)
* <http://halbtagsblog.de/> (L für Mathe/Physik)
* <https://sonnigeeinsichten.jimdo.com/> (L für Deutsch)
* <https://www.unterrichtsfreie-zeit.de/> (L für Deutsch/evang. Religion; Schwerpunkt des Blogs bzw. Podcasts liegt auf Zeitmanagement/Digitalisierung von schulischen Unterlagen)
* <https://www.mrsgreenflippedaus.de/> (L für Englisch/Geschichte)
* <https://emrich.in/> (GS-L)
* <https://bobblume.de/> (L für Englisch/Deutsch; hat neben seinem Blog auch einen Podcast, auf dem er interessante Personen aus dem Bildungsbereich interviewt)
* <https://mihajlovicfreiburg.com> (L für Chemie, Geschichte, Mathematik, Ethik an einer Realschule)
* <https://bankhoferedu.com/about-me/> (L für Englisch, Informatik und Digital Literacy) 
* <https://www.vedducation.de> (L an Oberschule, Pädagogischer Seminarleiter) 

---

### Block 1) Classroom-Management I

Classroomscreen.com ist ein sofort nutzbares Tool, dass dich beim Unterrichten in verschiedenen Phasen unterstützt und das Classroom-Management durch die Bündelung mehrerer Funktionen erleichtert.

* Lade das webbasierte Tool ‘ClassroomScreen’. Probiere mindestens vier Funktionen aus, indem du auf die entsprechenden Symbole klickst.
* Notiere drei Nutzungsszenarien für den Unterricht in einem deiner Fächer, in denen du Funktionen des Tools einsetzen könntest.
* Bewerte das Tool mit einer Schulnote und einer kurzen Begründung.

### Block 2) Classroom-Management II

Internetadressen, URLs, sind zum Teilen häufig zu lang - die Lösung für dieses Problem sind Websites zur Link-Verkürzung. Die Kurzlinks können namentlich sogar angepasst werden, zB an ein Thema, Fach oder Klasse.

* Rufe die Website ‘kurzelinks.de’ auf und erstelle einen Kurzlink zu einer Website deiner Wahl.
* Nutze die Funktion ‘+ einen Wunschlink angeben’ mit einem sinnvollen Stichwort.

### Block 3) Classroom-Management III

Mit QR-Codes lassen sich Informationen unterschiedlicher Art in der Form eines grafischen Bildchens verschlüsseln, welche dann mit einem QR-Code Scanner (App, in der iOS Kamera integriert) gelesen werden können. Das teils aufwändige Eintippen von URLs entfällt so.

* Suche dir aus der Spalte ‘QR-Codes’ (Padletlink #1) zwei Tools aus und verwandle den in Block 2) entstandenen Kurzlink in einen QR-Code. Prüfe, ob der Code funktioniert und zur Website führt. 
* QR-Codes können u.a. auch für Texte und Audio-Aufnahmen verwendet werden. Nutze die Website goqrme.com für die Textfunktion oder qwiqr.co.uk für Audio-Aufnahmen und schicke eine an eine*n Kommiliton*in gerichtete Nachricht.
* Notiere zwei Nutzungsszenarien für Textverschlüsselung oder Audio-Aufnahmen.

*Hinweis für Dozierende*: Studierende finden konkrete Anwendungsbeispiele ua auf den Blogs <https://sonnigeeinsichten.jimdo.com/> und <https://tollerunterricht.com/>

*Audio-Aufnahmen* betreffend können Studierende zB Folgendes nennen:

* Audio-Feedback zu Arbeiten von Schüler\*innen
* sprechende Plakate
* zusätzliche Erklärungen/Hinweise zu Arbeitsaufträgen im Sinne von Inklusion und Binnendifferenzierung
* Elterninformationen
* Hörelemente im Fremdsprachenunterricht

*Antwortmöglichkeiten zur Textverschlüsselung:*

* zusätzliche Erklärungen/Hinweise zu Arbeitsaufträgen im Sinne von Inklusion und Binnendifferenzierung
* Lösungen/-vorschläge im Sinne des individualisierten Lernens
* Schreibe eine kurze Elterninformation, in der du darlegst, weshalb sich die Schüler\*innen deiner Klasse einen QR-Code Scanner herunterladen sollen.

### Block 4) Diagnostik

Besonders beim Einstieg in ein neues Thema sind diagnostische Tools hilfreich, um das Vorwissen und vorhandene Assoziationen von Schüler\*innen zu reaktivieren und zu sammeln. Digitale Werkzeuge haben den Vorteil, dass diese Ideensammlungen  mit Präsentationstechnik für alle gut sichtbar visualisiert werden können, sodass nahtlos damit weitergearbeitet werden kann. Auch kurzes Feedback kann mit den Tools gegeben werden.

* Suche dir aus der Spalte ‘Ideen sammeln/Feedback’ zwei Tools aus, mit dem Ideen bzw. Assoziationen gesammelt werden können, zB padlet, mindmeister, oncoo und answergarden.  
* Gib in die Tools deine Assoziationen zum Thema ‘Lernen mit und über digitale(n) Medien’ bezogen auf deine Fächer ein.
* Lade einen Screenshot von deiner Ideensammlung in den Tools hoch. (*Hinweis für Dozierende:* Funktion muss bereitgestellt werden)
* Vergleiche die von dir getesteten Tools nach den Kriterien Bedienbarkeit und optischer Eindruck.
* Notiere, wie mit dem Produkt der Ideensammlung in einer Unterrichtssituation verfahren werden könnte.

### Block 5) Lernspiele

Zum Üben und Festigen von Lerninhalten verschiedener Fächer können Websites wie learningapps.org, h5p.org, kahoot.com genutzt werden.

* Suche dir eins der genannten Tools aus und erkunde die angebotenen Funktionen zB mit den Demo-Versionen.
* Bewerte das Tool mit einer Schulnote und einer kurzen Begründung.
* Notiere, in welchen Unterrichtsphasen und -szenarien du dir eine Nutzung des Tools vorstellen könntest.
* Positioniere dich begründet zu diesem Statement.

> Lehrer\*innen sollten diejenigen sein, die Aufgaben mit solchen Tools gestalten.

(*Hinweis für Dozierende:* Eine gute Idee kann es auch sein, Schüler\*innen zum kollaborativen Erstellen digitaler Aufgaben zu motivieren. Damit wird nicht nur das fachliche Verständnis der Schüler\*innen in besonderer Weise auf die Probe gestellt, sondern es werden auch Kompetenzen wie Teamfähigkeit gefördert.)

### Block 6) Kollaboratives Lernen

Tools wie das digitale Pinboard Padlet und Etherpads, zum Beispiel die Websites

* <https://cryptpad.fr/>
* <https://board.net/>
* <https://zumpad.zum.de/>
* <https://edupad.ch/>
* <https://unserpad.de/>

ermöglichen das kollaborative Arbeiten mehrerer Schüler\*innen an separaten Geräten an einem Produkt. Sie können beispielsweise für Gruppenarbeiten und gemeinsame Internetrecherchen oder zur gemeinsamen Überarbeitung von Texten eingesetzt werden.

* Öffne eins der Etherpads (mehrere der verlinkten funktionieren ohne Registrierung) und teile es mit einem Kommilitonen. Tauscht euch über eure Eindrücke sowie Vor- und Nachteile des Etherpads aus.
* Lies das folgende Szenario und tausche dich mit Kommiliton\*innen auf einem Etherpad aus, wie Maries Lernen mit Etherpads unterstützt werden kann.

> Szenario: Marie ist sportlich sehr erfolgreich und auch in der Schulzeit oft auf Wettkämpfen unterwegs.

* Beschreibt ein weiteres Nutzungsszenario.

### Block 7) Audio-Visuelles Lernen

Studien wie JIM und vielleicht auch deine eigene Erfahrung zeigen, dass Schüler\*innen in vielen Fällen ganz selbstverständlich Lernvideos oder auch Podcasts für ihr Lernen nutzen.
Dieser Block soll dich motivieren, selbst ein kurzes audio-visuelles Lernelement zu gestalten.

* Schaue ein Erklärvideo (zB von explainity). Erstelle einen Kriterienkatalog für das Erstellen eines guten Erklärvideos gemeinsam mit Kommilitonen auf einem Etherpad.
* Gestalte ein(en) 1-2-minütigen Podcast/Erklärvideo zum Thema deiner letzten Hausarbeit oder deines letzten Referats.
    * Du kannst dafür natürlich die vorhandenen technischen Möglichkeiten deines Smartphones/Tablets nutzen.
    * Quick & dirty kannst du auch mit dem Webtool  <https://website.mysimpleshow.com/> ein kurzes Erklärvideo erstellen.

### Block 8) Projekt

*Hinweis für Dozierende*:

Hier sollten Zeit und Raum für ein zusammenfassendem Projekt (fächer-/jahrgangsübergreifend) gegeben werden, in welches die in mehreren Blöcken angeeignete Kompetenzen einfließen sollen und Studierende kollaborativ arbeiten. Der Projektentwurf kann zB an Projekttagideen aus Lehrplänen angelehnt sein.
Bei dem Projekt sollte in besonderer Weise der Aspekt der Nachnutzbarkeit und des Teilens berücksichtigt werden! (zB öffentlicher Bereich in E-Portfolio, evtl mit OER verknüpfen)

## Deutsch lehren lernen ... in einer digitalen Welt. (Marie)

Willkommen bei der DeutschlehrerInnenbildung. Sie sind Dozierender oder angehende/r DeutschlehrerIn? Sie sind auf der Suche nach Arbeitshilfen zum eigenen Lernen und Lehren mit digitalen Medien - zur methodischen Gestaltung von Schulunterricht und Hochschullehre? Dann sind Sie hier genau richtig und dürfen sich in Ruhe umschauen, inspirieren lassen, entdecken, anwenden.

Nachfolgend aufgelistet gibt es für *(A) Dozierende der Fachdidaktik und Fachwissenschaft Deutsch* Materialien wie Lehr- und Methodenbeispiele sowie Methodensammlungen für die Gestaltung digital unterstützter Lehrveranstaltungen. Zum anderen gibt es für *(B) Studierende des Lehramtsfachs Deutsch* Materialien und Ideen zum Unterrichten u.a. in einer digitalen Welt. Eine allgemeine Sammlung *(C) weiterführender Tools und Ideen* ergänzt das Angebot für Studierende und Dozierende zum Lernen und Ausprobieren im Selbststudium oder die eigene fachliche und methodische Weiterbildung.

Künftig soll diese Sammlung noch weiter wachsen, daher wird es nach und nach noch weitere Beispiele und Materialien geben, die zu einer Lernkarte ausgebaut werden können.

### (A) Für Dozierende

#### Lehrmaterialien

* [Elixier](https://www.bildungsserver.de/elixier/elixier2_list.php?feldname2=Systematik%2FFach&feldinhalt2=%22Sprachen+und+Literatur%22&bool2=and&feldname3=Systematik%2FFach&feldinhalt3=%22Deutsch%22&bool3=and&&suche=erweitert&t=Suchen): Suchmaschine des DIPF für qualitätsgesicherte Bildungsmaterialien

* [Methodenspicker](https://d-3.germanistik.uni-halle.de/methodenspicker/): (Digitale) Lehrmethodenkarten des Projekts [D-3] mit Kurzbeschreibungen, Umsetzungstipps und Toolempfehlungen

* [Lernlandschaft](https://ilias.uni-halle.de/goto.php?target=cat_68293&client_id=unihalle): Methodensammlung des Projekts HET LSA für eine heterogenitätssensible Seminargestaltung

* [Methodenpool](http://methodenpool.uni-koeln.de/uebersicht.html): Umfangreiche Methodensammlung der Uni Köln v.a. klassischer, handlungsorientierter Methoden und Techniken

* [Lehrbausteine](https://d-3.germanistik.uni-halle.de/lehrbausteine/): Lehrbeispiele des Projekts [D-3] zur Vermittlung digitaler Kompetenzen

* [Sprachbildung](https://www.sprachen-bilden-chancen.de/index.php/sprachbildende-materialien/informationen-zur-entstehung-und-verwendung-der-aufgaben-und-lehrmaterialien): Materialien zur Sprachbildung der Universität Potsdam

* Webinare zu Lehr- und Methodenimpulsen

    * [Potentiale von Wikis für das kooperative Schreiben](https://www.youtube.com/watch?v=InjHNDNKhWQ)

    * [Prüfen, Messen, Wiegen: Leistungskontrollen und Notenermittlung an (Hoch)Schulen: analog und digital](https://www.youtube.com/watch?v=6Sam81p7NsE)

    * [Social Media im DU](https://www.youtube.com/watch?v=-pqaH77bB3I)

    * [Bildung visuell](https://www.youtube.com/watch?v=_kXBYe1pJ7c)

    * [Lernmodule, Kurse, Studiengang? Wege digitaler Medienbildung an der Universität](https://www.youtube.com/watch?v=GSuPDpFwI9s&list=PLFdAwreFK7ALDpqO9w45tm5dXCAKYJk6M&index=31)

### (B) Für Studierende

#### Unterrichtsmaterialien und Unterrichtsideen

* [ZUM-Unterrichten](https://unterrichten.zum.de/wiki/Deutsch)

* [Elixier](https://www.bildungsserver.de/elixier/elixier2_list.php?feldname2=Systematik%2FFach&feldinhalt2=%22Sprachen+und+Literatur%22&bool2=and&feldname3=Systematik%2FFach&feldinhalt3=%22Deutsch%22&bool3=and&&suche=erweitert&t=Suchen): Suchmaschine des DIPF für qualitätsgesicherte Bildungsmaterialien 

* [digi.komp](https://digikomp.at/index.php?id=595&L=0): Sammlung an Unterrichtsbeispielen des Bundes- und Koordinationszentrum eEducation Austria mit Filterfunktion digitaler Kompetenzen

* [Werkzeugkasten](https://www.medien-in-die-schule.de/werkzeugkaesten/)

* [Didaktische Werkstatt - Lehr- und Lernmaterialien](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/lehr-lernmaterialien/)

#### Unterrichtsentwürfe

* [Didaktische Werkstatt - Unterrichtsentwürfe](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/unterrichtsentwuerfe/)

#### Haus- und Abschlussarbeiten

* [Didaktische Werkstatt - Haus- und Abschlussarbeiten](https://home.uni-leipzig.de/fachdidaktik-germ/didaktische-werkstatt/haus-abschlussarbeiten/)

### (C) Weiterführendes

* Digitales Wörterbuch der deutsche Sprache: [https://www.dwds.de/](https://www.dwds.de/)

* Transkriptionssoftware: [https://www.audiotranskription.de/](https://www.audiotranskription.de/)

## LAdigital: Vergleich grundlegender Modelle digitaler Kompetenzen (Ines)

* [Link zum Dokument](https://docs.google.com/document/d/1kC_bfgI0CkaRfT_WhBUOH-TRMEIrcqTKtoftZ_tfn8A/edit)
